//Author: Farnam Khalili. 
//Description: to read a portion of the DDR from user space. 
// example to read :   sudo ./xsmll_mempri  0x41000008 8  --> this will print 8*32-bit of the DDR from the 
// base address of 0x41000008

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
       if (argc < 3) {
           printf("\r\n Usage: %s <phys_addr> <offset>\n", argv[0]);
           return 0;
       }

       off_t offset = strtoul(argv[1], NULL, 0); //after the name of the binary, the first argument.
       //the second argument is in words (32-bit), for example 4 means 4*4 bytes = 16 bytes. 
       
       size_t len_bytes = strtoul(argv[2], NULL, 0)<<2;//after the name of the binary. the second argument       is in word (32-bit), that is why we shifted it two times to the left to convert it to byte.
              // Truncate offset to a multiple of the page size, or mmap will fail.
              



       size_t pagesize = sysconf(_SC_PAGE_SIZE);
       off_t page_base = (offset / pagesize) * pagesize;
       off_t page_offset = offset - page_base;

       int fd = open("/dev/mem", O_SYNC);


       unsigned char *mem = mmap(NULL, page_offset + (len_bytes), PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, page_base);


                 
     
       size_t i,j;

       if (mem == MAP_FAILED) {
           perror("\n\r Can't map memory");
           return -1;
       }
        
       
       
       
                
                for (i = 0; i < (len_bytes); i=i+4){
                    if (j<7) {
                        printf("  %02x%02x%02x%02x"  , (int)mem[page_offset + i+3]  , (int)mem[page_offset + i+2]
                               ,(int)mem[page_offset + i+1]  , (int)mem[page_offset + i]
                    );
                        ++j;
                    } else { 
                        
                            printf("\n\r  %02x%02x%02x%02x",  (int)mem[page_offset + i+3]  , (int)mem[page_offset + i+2]
                              ,(int)mem[page_offset + i+1]  , (int)mem[page_offset + i]
                   
                           
                              );
                        j=0;
                   }
                }



      
       printf ("\n\r");
       return 0;
}


