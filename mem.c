/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
void display_mem(void* mem, int mem_size, int line_len) {
   /*
		mem 		- pointer to beggining of memory region to be printed
		mem_size 	- number of bytes mem points to
		line_len	- number of bytyes to display per line
   */

 	unsigned char* data = mem;
 	int full_lines = mem_size / line_len;
 	unsigned char* addr = mem;

 	for (int linno = 0; linno < full_lines; linno++) {
 		// Print Address
 		printf("0x%x\t", addr);

 		// Print Hex
 		for (int i = 0; i < line_len; i++) {
 			printf(" %02x", data[linno*line_len + i]);
 		}
 		printf("\t");

 		// Print Ascii
 		for (int i = 0; i < line_len; i++) {
 		    char c = data[linno*line_len + i];
 		    if ( 32 < c && c < 125) {
 			    printf(" %c", c);
 		    }
 		    else {
 		        printf(" .");
 		    }
 		}
 		printf("\n");

 		addr += line_len;
 	}
 	
 	// Print any remaining bytes that couldn't make a full line
 	int remaining = mem_size % line_len;
 	if (remaining > 0) {
 	    // Print Address
 	    printf("0x%x\t", addr);
 	    
 	    // Print Hex
 	    for (int i = 0; i < remaining; i++) {
 	        printf(" %02x", data[line_len*full_lines + i]);
    	}
    	for (int i = 0; i < line_len - remaining; i++) {
    	    printf("  ");
    	}
    	printf("\t");
    	
    	// Print Hex
 	    for (int i = 0; i < remaining; i++) {
 	        char c = data[line_len*full_lines + i];
 	        if ( 32 < c && c < 125) {
 			    printf(" %c", c);
 		    }
 		    else {
 		        printf(" .");
 		    }
    	}
    	printf("\n");
 	}
}
int main()
{
    char* test = malloc(10000);
    for (int i = 0 ; i < 100; i++) {
        test[i] = rand() % 127;
    }
    test[0] = 't';
    test[1] = 'e';
    test[2] = 's';
    test[3] = 't';
  
    display_mem(memory_pointer, 100, 7);
    return 0;
}


