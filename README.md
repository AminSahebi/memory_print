**Printing a chunk of memory, to test and see what is written in the memory**

I have gathered some simple codes to print the memory chunks,


---

## Warning
The mem.c just prints the portion of memory that MMU has allocated for you, so it's safe and it's passed through
the MMU, so kernel has control over it, you have to allocated and fill the allocated chunk then print it to see
how memory is filled.

The mem_print is a bit dangerous, because it bypasses the kernel MMU and goes directly to the Memory and print 
anything is there. So it must be executed by root permission and if sth goes wrong you can easily destroy the kernel.

